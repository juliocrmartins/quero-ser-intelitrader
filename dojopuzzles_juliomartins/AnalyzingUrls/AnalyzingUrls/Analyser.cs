﻿using AnalyzingUrls.Analysers;
using AnalyzingUrls.Models;

namespace AnalyzingUrls
{
    public class Analyser : BaseAnalyser
    {
        public Analyser(string url) : base(url)
        {
        }

        public override BaseAnalyser Analyse()
        {
            if (GetUrlType() == EnumType.TYPE_SSH)
                return new SshAnalyser(Url).Analyse();
            else if (GetUrlType() == EnumType.TYPE_URL)
                return new UrlAnalyser(Url).Analyse();
            else
                return this;
        }

        public override bool IsValid()
        {
            return GetUrlType() == EnumType.TYPE_SSH 
                || GetUrlType() == EnumType.TYPE_URL;
        }
    }
}
