﻿namespace AnalyzingUrls.Models
{
    public class SshModel : UrlBase
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}
