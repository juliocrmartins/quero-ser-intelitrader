﻿namespace AnalyzingUrls.Models
{
    public class UrlModel : UrlBase
    {
        public string Host { get; set; }
        public string Path { get; set; }
        public string[] Parameters { get; set; }
    }
}
