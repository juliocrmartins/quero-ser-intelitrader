﻿namespace AnalyzingUrls.Models
{
    public class UrlBase
    {
        public string Protocol { get; set; }
        public string Domain { get; set; }
    }
}
