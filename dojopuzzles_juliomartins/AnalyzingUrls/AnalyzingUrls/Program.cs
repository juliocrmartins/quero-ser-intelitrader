﻿using System;

namespace AnalyzingUrls
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite uma url (ssh/http/https): ");
            var url = Console.ReadLine();
            Console.WriteLine("\n=====================================");

            try
            {
                var analyser = new Analyser(url);

                if (analyser.GetUrlType() == Models.EnumType.TYPE_URL)
                {
                    var urlValues = analyser.Analyse().GetUrlValues();
                    Console.WriteLine($"{ nameof(urlValues.Protocol) }: { urlValues.Protocol }");
                    Console.WriteLine($"{ nameof(urlValues.Host) }: { urlValues.Host }");
                    Console.WriteLine($"{ nameof(urlValues.Domain) }: { urlValues.Domain }");
                    Console.WriteLine($"{ nameof(urlValues.Path) }: { urlValues.Path }");
                    Console.WriteLine($"{ nameof(urlValues.Parameters) }: { string.Join(", ", urlValues.Parameters) }");
                }
                else if (analyser.GetUrlType() == Models.EnumType.TYPE_SSH)
                {
                    var sshValues = analyser.Analyse().GetSshValues();
                    Console.WriteLine($"{ nameof(sshValues.Protocol) }: { sshValues.Protocol }");
                    Console.WriteLine($"{ nameof(sshValues.User) }: { sshValues.User }");
                    Console.WriteLine($"{ nameof(sshValues.Password) }: { sshValues.Password }");
                    Console.WriteLine($"{ nameof(sshValues.Domain) }: { sshValues.Domain }");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(string.Concat("An error ocurred: ", ex.Message));
            }

            Console.ReadKey();
        }
    }
}
