﻿using AnalyzingUrls.Models;
using System;

namespace AnalyzingUrls.Analysers
{
    public abstract class BaseAnalyser
    {
        protected const string URL_PROTOCOL_SEPARATOR = "//";

        public BaseAnalyser(string url)
        {
            Url = url;

            if (!IsValid())
                throw new Exception("This url isn't valid");
        }

        protected string Url;
        protected SshModel SshModel;
        protected UrlModel UrlModel;        

        public EnumType GetUrlType()
        {
            if (Url.StartsWith("ssh://"))
                return EnumType.TYPE_SSH;
            else if (Url.StartsWith("http://") || Url.StartsWith("https://"))
                return EnumType.TYPE_URL;

            return EnumType.UNDEFINED;
        }

        public SshModel GetSshValues()
        {
            return SshModel;
        }

        public UrlModel GetUrlValues()
        {
            return UrlModel;
        }        

        public abstract bool IsValid();

        public abstract BaseAnalyser Analyse();                   

        protected void ValidateDomain(string url)
        {
            if (!url.Contains("."))
                throw new Exception("This domain isn't valid");
        }

        protected string TreatProtocol(string protocol)
        {
            return protocol.Replace(":", "");
        }

        protected string ExtractPartFromUrl(
            string separator, 
            int idxSeparator, 
            ref string url)
        {
            var part = url.Substring(0, idxSeparator);
            url = url.Substring(idxSeparator + separator.Length, url.Length - (idxSeparator + separator.Length));

            return part;
        }

        protected string ExtractAllTextFromUrl(ref string url)
        {
            var part = url;
            url = string.Empty;
            return part;
        }
    }
}
