﻿using AnalyzingUrls.Models;
using System.Text.RegularExpressions;

namespace AnalyzingUrls.Analysers
{
    public class UrlAnalyser : BaseAnalyser
    {
        private const string VALIDATION_MATCH = @"^http(s)?://www\.([\w-]+.)+[\w-]+(/[\w- ./?%&=])?$";
        private const string URL_HOST_SEPARATOR = ".";
        private const string URL_DOMAIN_SEPARATOR = "/";
        private const string URL_PATH_SEPARATOR = "/";
        private const char URL_PARAMETER_SEPARATOR = '&';

        public UrlAnalyser(string url) : base(url)
        {
        }

        public override BaseAnalyser Analyse()
        {
            var url = TreatCurrentUrl();

            UrlModel = new UrlModel();
            UrlModel.Protocol = ExtractProtocolFromUrl(ref url);
            UrlModel.Host = ExtractHostFromUrl(ref url);
            UrlModel.Domain = ExtractDomainFromUrl(ref url);
            UrlModel.Path = ExtractPathFromUrl(ref url);
            UrlModel.Parameters = ExtractParametersFromUrl(ref url);

            return this;
        }        

        public override bool IsValid()
        {
            return GetUrlType() == EnumType.TYPE_URL 
                && Regex.IsMatch(Url, VALIDATION_MATCH);
        }
        
        private string ExtractProtocolFromUrl(ref string url)
        {
            var idxSeparator = url.IndexOf(URL_PROTOCOL_SEPARATOR);
            return TreatProtocol(ExtractPartFromUrl(URL_PROTOCOL_SEPARATOR, idxSeparator, ref url));                
        }

        private string ExtractHostFromUrl(ref string url)
        {
            var idxSeparator = url.IndexOf(URL_HOST_SEPARATOR);
            return ExtractPartFromUrl(URL_HOST_SEPARATOR, idxSeparator, ref url);
        }

        private string ExtractDomainFromUrl(ref string url)
        {
            ValidateDomain(url);
            var idxSeparator = url.IndexOf(URL_DOMAIN_SEPARATOR);
            if(idxSeparator > 0)
                return ExtractPartFromUrl(URL_DOMAIN_SEPARATOR, idxSeparator, ref url);
            else
                return ExtractAllTextFromUrl(ref url);
        }

        private string ExtractPathFromUrl(ref string url)
        {
            var idxSeparator = url.LastIndexOf(URL_PATH_SEPARATOR);

            if (url.Contains(URL_PATH_SEPARATOR))
                return ExtractPartFromUrl(URL_PATH_SEPARATOR, idxSeparator, ref url);
            else if (!UrlHasParameters(url) && url.Length > 0)
                return ExtractAllTextFromUrl(ref url);

            return string.Empty;
        }

        private string[] ExtractParametersFromUrl(ref string url)
        {
            if (url.Length > 0)
                return url.Split(URL_PARAMETER_SEPARATOR);

            return new string[] { };
        }

        private string TreatCurrentUrl()
        {
            return Url
                .Replace("%", "/")
                .Replace("?", "/");
        }

        private bool UrlHasParameters(string url)
        {
            return url.Contains("=") || url.Contains("&");
        }
    }
}
