﻿using AnalyzingUrls.Models;
using System.Text.RegularExpressions;

namespace AnalyzingUrls.Analysers
{
    public class SshAnalyser : BaseAnalyser
    {
        private const string VALIDATION_MATCH = @"^ssh?://([\w-]+.)+[\w-]?[./?%&=]([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})$";
        private const string URL_USER_SEPARATOR = "/";
        private const string URL_PASSWORD_SEPARATOR = "@";

        public SshAnalyser(string url) : base(url)
        {
        }

        public override BaseAnalyser Analyse()
        {
            var url = TreatCurrentUrl();

            SshModel = new SshModel();
            SshModel.Protocol = ExtractProtocolFromUrl(ref url);
            SshModel.User = ExtractUserFromUrl(ref url);
            SshModel.Password = ExtractPasswordFromUrl(ref url);
            SshModel.Domain = ExtractDomainFromUrl(ref url);
            
            return this;
        }

        public override bool IsValid()
        {
            return GetUrlType() == EnumType.TYPE_SSH
                && Regex.IsMatch(Url, VALIDATION_MATCH);
        }        

        private string ExtractProtocolFromUrl(ref string url)
        {
            var separator = URL_PROTOCOL_SEPARATOR;
            var idxSeparator = url.IndexOf(separator);

            return TreatProtocol(ExtractPartFromUrl(separator, idxSeparator, ref url));                
        }

        private string ExtractUserFromUrl(ref string url)
        {
            var idxSeparator = url.IndexOf(URL_USER_SEPARATOR);
            return ExtractPartFromUrl(URL_USER_SEPARATOR, idxSeparator, ref url);
        }

        private string ExtractPasswordFromUrl(ref string url)
        {
            var idxSeparator = url.IndexOf(URL_PASSWORD_SEPARATOR);
            return ExtractPartFromUrl(URL_PASSWORD_SEPARATOR, idxSeparator, ref url);
        }

        private string ExtractDomainFromUrl(ref string url)
        {
            ValidateDomain(url);
            return ExtractAllTextFromUrl(ref url);
        }

        private string TreatCurrentUrl()
        {
            return Url
                .Replace("%", "/");
        }
    }
}
