﻿namespace DiscoverKiller.Models
{
    public class Local : Evidence
    {
        public const int INCORRECT_VALUE = 2;

        public override int GetIncorrectValue()
        {
            return INCORRECT_VALUE;
        }
    }
}
