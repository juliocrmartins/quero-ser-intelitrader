﻿namespace DiscoverKiller.Models
{
    public class Suspect : Evidence
    {
        public const int INCORRECT_VALUE = 1;

        public override int GetIncorrectValue()
        {
            return INCORRECT_VALUE;
        }
    }
}
