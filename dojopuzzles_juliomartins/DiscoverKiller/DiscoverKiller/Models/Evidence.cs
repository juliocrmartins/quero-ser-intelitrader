﻿namespace DiscoverKiller.Models
{
    public abstract class Evidence
    {
        public abstract int GetIncorrectValue();

        public int Number { get; set; }
        public string Name { get; set; }
    }
}
