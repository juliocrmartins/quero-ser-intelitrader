﻿namespace DiscoverKiller.Models
{
    public class Weapon : Evidence
    {
        public const int INCORRECT_VALUE = 3;

        public override int GetIncorrectValue()
        {
            return INCORRECT_VALUE;
        }
    }
}
