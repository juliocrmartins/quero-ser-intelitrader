﻿namespace DiscoverKiller.Models
{
    public class InvestigationCase
    {
        public Suspect Suspect { get; set; }
        public Local Local { get; set; }
        public Weapon Weapon { get; set; }
    }
}

