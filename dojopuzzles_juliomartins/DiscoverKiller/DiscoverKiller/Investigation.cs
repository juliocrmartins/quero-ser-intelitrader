﻿using DiscoverKiller.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscoverKiller
{
    public class Investigation
    {
        private List<int[]> PreviousTheories;
        private List<int> TheoryInvalidNumbers { get; set; }

        private List<int> ProbablySuspectNumbers;
        private List<int> ProbablyLocalNumbers;
        private List<int> ProbablyWeaponNumbers;

        private int ValidSuspectNumber;
        private int ValidLocalNumber;
        private int ValidWeaponNumber;

        public const int THEORY_NUMBERS = 3;

        public Investigation()
        {
            PreviousTheories = new List<int[]>();
            TheoryInvalidNumbers = new List<int>();
            ProbablySuspectNumbers = new List<int>();
            ProbablyLocalNumbers = new List<int>();
            ProbablyWeaponNumbers = new List<int>();
        }

        public List<Suspect> GetAllSuspects()
        {
            return new List<Suspect>
            {
                new Suspect { Number = 1, Name = "Charles B. Abbage" },
                new Suspect { Number = 2, Name = "Donald Duck Knuth" },
                new Suspect { Number = 3, Name = "Ada L. Ovelace" },
                new Suspect { Number = 4, Name = "Alan T. Uring" },
                new Suspect { Number = 5, Name = "Ivar J. Acobson" },
                new Suspect { Number = 6, Name = "Ras Mus Ler Dorf" },
            };
        }

        public List<Local> GetAllLocals()
        {
            return new List<Local>
            {
                new Local { Number = 1, Name = "Redmond" },
                new Local { Number = 2, Name = "Palo Alto" },
                new Local { Number = 3, Name = "San Francisco" },
                new Local { Number = 4, Name = "Tokio" },
                new Local { Number = 5, Name = "Restaurante no Fim do Universo" },
                new Local { Number = 6, Name = "São Paulo" },
                new Local { Number = 7, Name = "Cupertino" },
                new Local { Number = 8, Name = "Helsinki" },
                new Local { Number = 9, Name = "Maida Vale" },
                new Local { Number = 10, Name = "Toronto" },
            };
        }

        public List<Weapon> GetAllWeapons()
        {
            return new List<Weapon>
            {
                new Weapon { Number = 1, Name = "Peixeira"},
                new Weapon { Number = 2, Name = "DynaTAC 8000X (o primeiro aparelho celular do mundo)"},
                new Weapon { Number = 3, Name = "Trezoitão"},
                new Weapon { Number = 4, Name = "Trebuchet"},
                new Weapon { Number = 5, Name = "Maça"},
                new Weapon { Number = 6, Name = "Gládio"},
            };
        }

        public int[] GetTheoryNumbers()
        {
            var theory = null as int[];
            var alreadyInPreviousTheories = false;

            do
            {
                theory = new int[] {
                    RandomSuspect().Number,
                    RandomLocal().Number,
                    RandomWeapon().Number
                };

                alreadyInPreviousTheories = CheckIfAlreadyInPreviousTheories(theory);

            } while (alreadyInPreviousTheories);

            PreviousTheories.Add(theory);

            return theory;
        }

        public void SetTheoryInvalidNumbers(int[] numbers)
        {
            ProbablySuspectNumbers = ProbablySuspectNumbers.Where(n => !numbers.Contains(n)).ToList();
            ProbablyLocalNumbers = ProbablyLocalNumbers.Where(n => !numbers.Contains(n)).ToList();
            ProbablyWeaponNumbers = ProbablyWeaponNumbers.Where(n => !numbers.Contains(n)).ToList();

            TheoryInvalidNumbers.AddRange(numbers);
        }

        public List<int> GetTheoryInvalidNumbers()
        {
            return TheoryInvalidNumbers;
        }

        public void ClearAndSetProbablySuspectNumbers(int[] numbers)
        {
            ProbablySuspectNumbers.Clear();
            ProbablySuspectNumbers.AddRange(
                numbers
                .Where(n => GetAllSuspects().Select(s => s.Number).Contains(n) 
                    && !ProbablySuspectNumbers.Contains(n))
            );
        }

        public void ClearAndSetProbablyLocalNumbers(int[] numbers)
        {
            ProbablyLocalNumbers.Clear();
            ProbablyLocalNumbers.AddRange(
                numbers
                .Where(n => GetAllLocals().Select(l => l.Number).Contains(n)
                    && !ProbablyLocalNumbers.Contains(n))
            );
        }

        public void ClearAndSetProbablyWeaponNumbers(int[] numbers)
        {
            ProbablyWeaponNumbers.Clear();
            ProbablyWeaponNumbers.AddRange(
                numbers
                .Where(n => GetAllWeapons().Select(w => w.Number).Contains(n)
                    && !ProbablyWeaponNumbers.Contains(n))
            );
        }

        public void SetValidSuspectNumber(int number)
        {
            ValidSuspectNumber = number;
        }

        public void SetValidLocalNumber(int number)
        {
            ValidLocalNumber = number;
        }

        public void SetValidWeaponNumber(int number)
        {
            ValidWeaponNumber = number;
        }

        private bool CheckIfAlreadyInPreviousTheories(int[] theory)
        {
            return PreviousTheories
                    .Select(p => string.Join(string.Empty, p.OrderBy(v => v)))
                        .Any(v => v == string.Join(string.Empty, theory.OrderBy(t => t)));
        }

        private Suspect RandomSuspect()
        {
            var isInvalidNumber = false;
            var number = default(int);

            do
            {
                var random = new Random();

                if (ValidSuspectNumber > 0)
                {
                    number = ValidSuspectNumber;
                    break;
                }
                else if (ProbablySuspectNumbers.Any())
                {
                    var idxRandom = random.Next(ProbablySuspectNumbers.Count());
                    number = ProbablySuspectNumbers[idxRandom];
                }
                else
                {
                    var idxRandom = random.Next(GetAllSuspects().Count());
                    number = GetAllSuspects()[idxRandom].Number;
                }

                isInvalidNumber = TheoryInvalidNumbers.Contains(number);
            }
            while (isInvalidNumber);

            return GetAllSuspects().First(s => s.Number == number);
        }

        private Local RandomLocal()
        {
            var isInvalidNumber = false;
            var number = default(int);

            do
            {
                var random = new Random();

                if (ValidLocalNumber > 0)
                {
                    number = ValidLocalNumber;
                    break;
                }
                else if (ProbablyLocalNumbers.Any())
                {
                    var idxRandom = random.Next(ProbablyLocalNumbers.Count());
                    number = ProbablyLocalNumbers[idxRandom];
                }
                else
                {
                    var idxRandom = random.Next(GetAllLocals().Count());
                    number = GetAllLocals()[idxRandom].Number;
                }

                isInvalidNumber = TheoryInvalidNumbers.Contains(number);
            }
            while (isInvalidNumber);

            return GetAllLocals().First(s => s.Number == number);
        }

        private Weapon RandomWeapon()
        {
            var isInvalidNumber = false;
            var number = default(int);

            do
            {
                var random = new Random();

                if (ValidWeaponNumber > 0)
                {
                    number = ValidWeaponNumber;
                    break;
                }
                else if (ProbablyWeaponNumbers.Any())
                {
                    var idxRandom = random.Next(ProbablyWeaponNumbers.Count());
                    number = ProbablyWeaponNumbers[idxRandom];
                }
                else
                {
                    var idxRandom = random.Next(GetAllWeapons().Count());
                    number = GetAllWeapons()[idxRandom].Number;
                }

                isInvalidNumber = TheoryInvalidNumbers.Contains(number);
            }
            while (isInvalidNumber);

            return GetAllWeapons().First(s => s.Number == number);
        }
    }
}
