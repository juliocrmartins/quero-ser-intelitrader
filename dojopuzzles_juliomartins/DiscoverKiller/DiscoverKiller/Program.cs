﻿using DiscoverKiller.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscoverKiller
{
    class Program
    {       
        static void Main(string[] args)
        {
            var investigation = new Investigation();

            var suspect = DrawAndGetValueInput(
                "Escolha um suspeito: ", 
                investigation
                    .GetAllSuspects()
                    .Select(s => 
                        new Tuple<int, string>(s.Number, s.Name)
                    )
                    .ToList());

            var local = DrawAndGetValueInput(
                "Escolha um local: ", 
                investigation
                    .GetAllLocals()
                    .Select(l => 
                        new Tuple<int, string>(l.Number, l.Name)
                    )
                    .ToList());

            var weapon = DrawAndGetValueInput(
                "Escolha uma arma: ", 
                investigation
                    .GetAllWeapons()
                    .Select(w => 
                        new Tuple<int, string>(w.Number, w.Name)
                    )
                    .ToList());

            var solution = new InvestigationCase
            {                
                Suspect = investigation.GetAllSuspects().First(s => s.Number == suspect),
                Local = investigation.GetAllLocals().First(l => l.Number == local),
                Weapon = investigation.GetAllWeapons().First(w => w.Number == weapon)
            };

            Console.Clear();
            Console.WriteLine($"O assassino é { solution.Suspect.Name } usando um { solution.Weapon.Name } em { solution.Local.Name }\n");

            var theoryIsValid = false;
            var theoryCounters = 0;

            do
            {
                theoryCounters++;                

                var resultNumbers = new List<int>();
                var resultLabel = string.Empty;

                var theoryNumbers = investigation.GetTheoryNumbers();

                var investigationTheory = new InvestigationTheory(theoryNumbers, solution);

                theoryIsValid = investigationTheory.ChecksIfTheoryIsValid(ref resultNumbers, ref resultLabel);

                if (!theoryIsValid)
                    investigationTheory.InvestigateInvalidTheory(investigation, ref resultNumbers, ref resultLabel);

                Console.WriteLine($"Teoria: { string.Join(", ", theoryNumbers) }");
                Console.WriteLine($"Retorno: { string.Join(", ou ", resultNumbers) } { resultLabel } \n");

            }
            while (!theoryIsValid);            

            Console.WriteLine($"Numero de Teorias: {theoryCounters}\n");

            Console.ReadKey();
        }

        private static int DrawAndGetValueInput(string inputLabel, List<Tuple<int, string>> comboValues)
        {
            var isValid = true;
            var number = default(int);

            do
            {
                Console.Clear();

                foreach (var item in comboValues)
                {
                    Console.WriteLine($"{item.Item1}. {item.Item2}");
                }

                Console.WriteLine();

                if (!isValid)
                    Console.WriteLine("(Digite um número válido)");

                Console.WriteLine(inputLabel);

                var input = Console.ReadLine();

                isValid = Int32.TryParse(input, out number);

                if (isValid)
                    isValid = comboValues.Any(n => n.Item1 == number);
            }
            while (!isValid);

            return number;
        }
    }
}
