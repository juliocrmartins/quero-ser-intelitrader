﻿using DiscoverKiller.Models;
using System.Collections.Generic;
using System.Linq;

namespace DiscoverKiller
{
    public class InvestigationTheory
    {
        private readonly int[] TheoryNumbers;
        private readonly InvestigationCase Solution;

        public InvestigationTheory(int[] theoryNumbers, InvestigationCase solution)
        {
            TheoryNumbers = theoryNumbers;
            Solution = solution;
        }        

        public void InvestigateInvalidTheory(
            Investigation investigation,             
            ref List<int> resultNumbers,
            ref string resultLabel)
        {
            if (resultNumbers.Count() == 1)
                InvestigateOnlyOneIsIncorrect(investigation, ref resultNumbers, ref resultLabel);
            else if (resultNumbers.Count() == Investigation.THEORY_NUMBERS - 1)
                InvestigateOnlyOneIsValid(investigation, ref resultNumbers, ref resultLabel);
            else if (resultNumbers.Count() == Investigation.THEORY_NUMBERS)
                InvestigateAllIsIncorrect(investigation, ref resultNumbers, ref resultLabel);
        }

        public bool ChecksIfTheoryIsValid(
            ref List<int> resultNumbers,
            ref string resultLabel)
        {
            var hasSuspectNumber = false;
            var hasLocalNumber = false;
            var hasWeaponNumber = false;       

            foreach (var number in TheoryNumbers)
            {
                if (!hasSuspectNumber && Solution.Suspect.Number == number)
                {
                    hasSuspectNumber = true;
                    continue;
                }

                if (!hasLocalNumber && Solution.Local.Number == number)
                {
                    hasLocalNumber = true;
                    continue;
                }

                if (!hasWeaponNumber && Solution.Weapon.Number == number)
                {
                    hasWeaponNumber = true;
                    continue;
                }
            }

            if (!hasSuspectNumber)
                resultNumbers.Add(Solution.Suspect.GetIncorrectValue());

            if (!hasLocalNumber)
                resultNumbers.Add(Solution.Local.GetIncorrectValue());

            if (!hasWeaponNumber)
                resultNumbers.Add(Solution.Weapon.GetIncorrectValue());

            if (!resultNumbers.Any())
            {
                resultLabel = "(todos corretos, você solucionou o caso)";
                resultNumbers.Add(0);
                return true;
            }
            else
                return false;
        }

        private void InvestigateOnlyOneIsIncorrect(
            Investigation investigation,
            ref List<int> resultNumbers,
            ref string resultLabel)
        {
            if (resultNumbers.Contains(Solution.Suspect.GetIncorrectValue()))
                resultLabel = "(Somente o assassino está incorreto)";
            else if (resultNumbers.Contains(Solution.Local.GetIncorrectValue()))
                resultLabel = "(Somente o local está incorreto)";
            else if (resultNumbers.Contains(Solution.Weapon.GetIncorrectValue()))
                resultLabel = "(Somente a arma está incorreta)";
        }

        private void InvestigateOnlyOneIsValid(
            Investigation investigation,
            ref List<int> resultNumbers,
            ref string resultLabel)
        {
            var validNumbers = TheoryNumbers
                .Where(n => !investigation.GetTheoryInvalidNumbers().Contains(n));

            if (resultNumbers.Contains(Solution.Local.GetIncorrectValue())
                && resultNumbers.Contains(Solution.Weapon.GetIncorrectValue()))
            {
                resultLabel = "(somente o suspeito está correto)";

                if (validNumbers.Count() == 1)
                    investigation.SetValidSuspectNumber(validNumbers.First());
                else
                    investigation.ClearAndSetProbablySuspectNumbers(TheoryNumbers.Distinct().ToArray());
            }
            else if (resultNumbers.Contains(Solution.Suspect.GetIncorrectValue())
                && resultNumbers.Contains(Solution.Weapon.GetIncorrectValue()))
            {
                resultLabel = "(somente o local está correto)";

                if (validNumbers.Count() == 1)
                    investigation.SetValidLocalNumber(validNumbers.First());
                else
                    investigation.ClearAndSetProbablyLocalNumbers(TheoryNumbers.Distinct().ToArray());
            }
            else if (resultNumbers.Contains(Solution.Suspect.GetIncorrectValue())
                && resultNumbers.Contains(Solution.Local.GetIncorrectValue()))
            {
                resultLabel = "(somente a arma está correta)";

                if (validNumbers.Count() == 1)
                    investigation.SetValidWeaponNumber(validNumbers.First());
                else
                    investigation.ClearAndSetProbablyWeaponNumbers(TheoryNumbers.Distinct().ToArray());
            }
        }

        private void InvestigateAllIsIncorrect(
            Investigation investigation,
            ref List<int> resultNumbers,
            ref string resultLabel)
        {
            resultLabel = "(todos estão incorretos)";

            investigation.SetTheoryInvalidNumbers(TheoryNumbers.Distinct().ToArray());
        }
    }
}
