﻿using NavalBattle.Model;
using System;

namespace NavalBattle
{
    class Program
    {
        static void Main(string[] args)
        {
            var player1 = string.Empty;
            var player2 = string.Empty;

            BoardDraw.DrawTitle();

            DrawPlayersInputs(ref player1, ref player2);

            var render = new BoardRender();
            var engine = new BoardEngine(render);
            var draw = new BoardDraw();

            render.PrepareBoard(player1, player2);

            do
            {
                var board = render.GetOpponentBoard(render.GetPlayerTurn());
                var currentPlayer = render
                    .GetPlayerBoard(render.GetPlayerTurn())
                    .GetPlayerName();

                DrawTitleAndBoardItems(draw, board);

                var input = DrawCoordinatesInputs(currentPlayer);

                engine.CheckAttack(board, new Coordinate(input.GetCoordinateX(), input.GetCoordinateY()));
                render.SetPlayerTurn();

                DrawTitleAndBoardItems(draw, board);
                DrawEndAttack(currentPlayer);
            }
            while (!engine.IsGameOver());

            Console.Clear();
            BoardDraw.DrawTitle();

            var winner = engine.GetWinnerPlayer();
            var winnerBoard = render.GetPlayerBoard(winner);
            var opponentBoard = render.GetOpponentBoard(winner);

            draw.DrawWinner(winnerBoard.GetPlayerName());

            DrawBoardGameOver(draw, opponentBoard);

            Console.ReadKey();
        }

        private static void DrawPlayersInputs(ref string player1, ref string player2)
        {
            Console.Write("Digite o nome do Player 1: ");
            player1 = Console.ReadLine();
            Console.Write("Digite o nome do Player 2: ");
            player2 = Console.ReadLine();
        }       

        private static Coordinate DrawCoordinatesInputs(string currentPlayer)
        {
            Console.WriteLine($"{ currentPlayer } é a sua vez de atacar!");

            var x = DrawAndGetCoordinateValueInput("Digite a coordenada (x): ", Board.NUMBER_OF_COLUMNS - 1);
            var y = DrawAndGetCoordinateValueInput("Digite a coordenada (y): ", Board.NUMBER_OF_ROWS - 1);

            return new Coordinate(x, y);
        }

        private static int DrawAndGetCoordinateValueInput(string inputLabel, int maxNumber)
        {
            var isValidCoordinate = true;
            var coordinate = default(int);

            do
            {
                if (isValidCoordinate)
                {
                    Console.Write(inputLabel);
                }
                else
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    ClearCurrentConsoleLine();
                    Console.Write(inputLabel);
                }

                var input = Console.ReadLine();
                isValidCoordinate = Int32.TryParse(input, out coordinate);

                if (isValidCoordinate)
                    isValidCoordinate = coordinate <= maxNumber;

            } while (!isValidCoordinate);

            return coordinate;
        }

        private static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        private static void DrawTitleAndBoardItems(BoardDraw draw, Board board)
        {
            Console.Clear();
            BoardDraw.DrawTitle();

            Console.WriteLine($"Mapa de: {board.GetPlayerName()}");
            draw.DrawBoard(board);
            draw.DrawBoatsStatistics(board);            
        }

        private static void DrawEndAttack(string currentPlayer)
        {
            Console.WriteLine($"{ currentPlayer }. Pressione enter para terminar sua vez");
            Console.ReadKey();
        }

        private static void DrawBoardGameOver(BoardDraw draw, Board opponentBoard)
        {
            Console.WriteLine($"Mapa de: {opponentBoard.GetPlayerName()}");
            draw.DrawBoard(opponentBoard, true);
        }
    }
}
