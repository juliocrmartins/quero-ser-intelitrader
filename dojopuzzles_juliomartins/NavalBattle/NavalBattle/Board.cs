﻿using NavalBattle.Boats;
using NavalBattle.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NavalBattle
{
    public class Board
    {
        public Board(string playerName)
        {
            AircraftCarriers = new List<AircraftCarrier>();
            Battleships = new List<Battleship>();
            Destroyers = new List<Destroyer>();
            Submarines = new List<Submarine>();
            PatrolBoats = new List<PatrolBoat>();
            AttacksCoordinates = new List<Coordinate>();

            PlayerName = playerName;
        }

        private const int NUMBER_OF_AIRCRAFT_CARRIERS = 1;
        private const int NUMBER_OF_BATTLESHIPS = 1;
        private const int NUMBER_OF_DESTROYERS = 1;
        private const int NUMBER_OF_SUBMARINES = 1;
        private const int NUMBER_OF_PATROLBOATS = 1;
        private string PlayerName;
        private List<Coordinate> AttacksCoordinates;

        public const int NUMBER_OF_COLUMNS = 10;
        public const int NUMBER_OF_ROWS = 10;

        public List<AircraftCarrier> AircraftCarriers { get; set; }
        public List<Battleship> Battleships { get; set; }
        public List<Destroyer> Destroyers { get; set; }
        public List<Submarine> Submarines { get; set; }
        public List<PatrolBoat> PatrolBoats { get; set; }

        public List<Coordinate> GetAttackCoordinates()
        {
            return AttacksCoordinates;
        }

        public void Attack(Coordinate coordinate)
        {
            AttacksCoordinates.Add(coordinate);
        }

        public string GetPlayerName()
        {
            return PlayerName;
        }        

        public void CreateAllBoats()
        {
            CreateBoats<AircraftCarrier>(NUMBER_OF_AIRCRAFT_CARRIERS);
            CreateBoats<Battleship>(NUMBER_OF_BATTLESHIPS);
            CreateBoats<Destroyer>(NUMBER_OF_DESTROYERS);
            CreateBoats<Submarine>(NUMBER_OF_SUBMARINES);
            CreateBoats<PatrolBoat>(NUMBER_OF_PATROLBOATS);            
        }

        private Coordinate RandomInitialPositionCoordinate<T>(EnumDirection direction) where T : Boat
        {
            var random = new Random();

            var boat = (T)Activator.CreateInstance(typeof(T));

            var numberOfColumns = NUMBER_OF_COLUMNS;
            var numberOfRows = NUMBER_OF_ROWS;

            if (direction == EnumDirection.HORIZONTAL)
                numberOfColumns = NUMBER_OF_COLUMNS - boat.GetLength();
            else
                numberOfRows = NUMBER_OF_ROWS - boat.GetLength();

            var x = random.Next(0, numberOfColumns);
            var y = random.Next(0, numberOfRows);

            return new Coordinate(x, y);
        }

        private EnumDirection RandomDirection()
        {
            var random = new Random();
            var value = random.Next(0, 9);

            return value < 5 ? EnumDirection.VERTICAL : EnumDirection.HORIZONTAL;
        }

        private void CreateBoats<T>(int numberOfBoats) where T: Boat
        {
            var createdBoats = 0;

            do
            {
                var boat = (T)Activator.CreateInstance(typeof(T));
                var direction = RandomDirection();

                var positionCoordinates = GetCreatedBoatPosition(
                    boat.GetLength(),
                    RandomInitialPositionCoordinate<T>(direction),
                    direction);

                boat.SetPosition(positionCoordinates);

                if (CanCreateBoatWithThisCoordinates(positionCoordinates))
                {
                    if (boat.GetType() == typeof(AircraftCarrier))
                        AircraftCarriers.Add(boat as AircraftCarrier);
                    else if (boat.GetType() == typeof(Battleship))
                        Battleships.Add(boat as Battleship);
                    else if (boat.GetType() == typeof(Destroyer))
                        Destroyers.Add(boat as Destroyer);
                    else if (boat.GetType() == typeof(Submarine))
                        Submarines.Add(boat as Submarine);
                    else if (boat.GetType() == typeof(PatrolBoat))
                        PatrolBoats.Add(boat as PatrolBoat);

                    createdBoats++;
                }
            }
            while (createdBoats < numberOfBoats);
        }

        private bool CanCreateBoatWithThisCoordinates(List<Coordinate> coordinates)
        {
            foreach(var current in coordinates)
            {
                if (GetAllBoatsCoordinates()
                    .Any(c => c.GetCoordinateX() == current.GetCoordinateX() 
                        && c.GetCoordinateY() == current.GetCoordinateY()))
                    return false;
            }

            return true;
        }

        private List<Coordinate> GetCreatedBoatPosition(            
            int boatLength, 
            Coordinate initialPositionCoordinate,           
            EnumDirection direction)
        {
            var positionCoordinates = new List<Coordinate>();

            positionCoordinates.Add(initialPositionCoordinate);
            var x = initialPositionCoordinate.GetCoordinateX();
            var y = initialPositionCoordinate.GetCoordinateY();

            for (var i = 1; i < boatLength; i++)
            {
                if (direction == EnumDirection.HORIZONTAL)
                    x++;
                else
                    y++;

                positionCoordinates.Add(new Coordinate(x, y));
            }

            return positionCoordinates;
        }

        public List<Boat> GetAllBoats()
        {
            var boats = new List<Boat>();

            boats.AddRange(AircraftCarriers);
            boats.AddRange(Battleships);
            boats.AddRange(Destroyers);
            boats.AddRange(Submarines);
            boats.AddRange(PatrolBoats);

            return boats;
        }

        public List<BoatCoordinate> GetAllBoatsCoordinatesWithBoat()
        {
            var boatCoordinates = new List<BoatCoordinate>();

            var boats = GetAllBoats();

            foreach(var current in boats)
            {
                boatCoordinates.AddRange(
                    current.GetPosition()
                    .Select(c =>
                        new BoatCoordinate
                        {
                            Boat = current,
                            Coordinate = c
                        })
                    );
            }
            
            return boatCoordinates;
        }

        private List<Coordinate> GetAllBoatsCoordinates()
        {
            var boats = GetAllBoats();

            var coordinates = new List<Coordinate>();

            foreach(var current in boats)
            {
                coordinates.AddRange(current.GetPosition());
            }            

            return coordinates;
        }
    }
}
