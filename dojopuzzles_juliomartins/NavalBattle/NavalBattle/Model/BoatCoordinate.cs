﻿using NavalBattle.Boats;

namespace NavalBattle.Model
{
    public class BoatCoordinate
    {
        public Boat Boat { get; set; }
        public Coordinate Coordinate { get; set; }
    }
}
