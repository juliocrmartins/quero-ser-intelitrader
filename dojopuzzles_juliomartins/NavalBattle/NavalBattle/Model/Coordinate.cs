﻿namespace NavalBattle.Model
{
    public class Coordinate
    {
        private int X;
        private int Y;

        public Coordinate(int x, int y)
        {
            SetCoordinates(x, y);
        }

        public void SetCoordinates(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int GetCoordinateX()
        {
            return X;
        }

        public int GetCoordinateY()
        {
            return Y;
        }
    }
}
