﻿using System;
using System.Linq;

namespace NavalBattle
{
    public class BoardDraw : BoardRender
    {
        public void DrawBoard(
            Board board, 
            bool showAllBoats = false)
        {
            Console.WriteLine("(y)");

            for (var y = 0; y < Board.NUMBER_OF_ROWS; y++)
            {
                Console.Write($" {y}| ");

                for (var x = 0; x < Board.NUMBER_OF_COLUMNS; x++)
                {
                    var value = GetValueToDraw(x, y, board, showAllBoats);

                    Console.Write($"{ value } ");
                }

                Console.WriteLine();
            }

            Console.Write("    ");
            for (var x = 0; x < Board.NUMBER_OF_ROWS; x++)
            {
                Console.Write($"--");
            }
            Console.WriteLine();

            Console.Write("    ");
            for (var x = 0; x < Board.NUMBER_OF_ROWS; x++)
            {
                Console.Write($"{x} ");
            }
            Console.Write("(x)\n\n");
        }

        public void DrawBoatsStatistics(Board board)
        {
            Console.WriteLine("\n-----------------------------------");
            Console.WriteLine("--          Estatísticas         --");
            Console.WriteLine("-----------------------------------");

            Console.WriteLine($"-- Porta-Aviões      : { board.AircraftCarriers.Count(b => !b.WasDestroyed()) }         --");
            Console.WriteLine($"-- Encouraçado       : { board.Battleships.Count(b => !b.WasDestroyed()) }         --");
            Console.WriteLine($"-- Submarino         : { board.Submarines.Count(b => !b.WasDestroyed()) }         --");
            Console.WriteLine($"-- Destroyer         : { board.Destroyers.Count(b => !b.WasDestroyed()) }         --");
            Console.WriteLine($"-- Barco de Patrulha : { board.PatrolBoats.Count(b => !b.WasDestroyed()) }         --");

            Console.WriteLine("-----------------------------------\n");
        }

        public void DrawWinner(string playerName)
        {
            Console.WriteLine($"**************************************************************************");
            Console.WriteLine($"     Parabéns { playerName }! Você foi o vencedor    ");
            Console.WriteLine($"**************************************************************************\n");
        }

        public static void DrawTitle()
        {
            Console.WriteLine(@"
                
            ||    ||  ||    ||   ||  ||      ||           |||||    ||    |||||||| ||      ||      |||||||     
            ||||  ||  ||||   ||  ||  ||||    ||           ||   ||  ||||     ||    ||      ||      ||     
            || || ||  || ||   || ||  || ||   ||           |||||    || ||    ||    ||      ||      ||||||     
            ||  ||||  ||||||   ||||  ||||||  ||           ||   ||  ||||||   ||    ||      ||      ||     
            ||   |||  ||   ||   |||  ||   || |||||||      |||||    ||   ||  ||    ||||||| ||||||| |||||||

            ");
        }
    }
}
