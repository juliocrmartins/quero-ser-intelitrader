﻿using NavalBattle.Model;
using System.Linq;

namespace NavalBattle
{
    public class BoardRender
    {
        private const char HIT_BOAT_VALUE = 'x';
        private const char HIT_SEA_VALUE = '*';
        private const char SEA_VALUE = '=';

        private bool IsPlayer1Turn;
        private Board BoardPlayer1;
        private Board BoardPlayer2;

        public BoardRender()
        {
            IsPlayer1Turn = true;
        }

        public void SetPlayerTurn()
        {
            IsPlayer1Turn = !IsPlayer1Turn;
        }        

        public EnumPlayers GetPlayerTurn()
        {
            return IsPlayer1Turn ? EnumPlayers.PLAYER1 : EnumPlayers.PLAYER2;
        }

        public Board GetOpponentBoard(EnumPlayers currentPlayer)
        {
            return currentPlayer == EnumPlayers.PLAYER1 
                ? BoardPlayer2 
                : BoardPlayer1;
        }

        public Board GetPlayerBoard(EnumPlayers currentPlayer)
        {
            return currentPlayer == EnumPlayers.PLAYER1
                ? BoardPlayer1
                : BoardPlayer2;
        }

        public void PrepareBoard(
            string player1, 
            string player2)
        {
            BoardPlayer1 = new Board(player1);
            BoardPlayer1.CreateAllBoats();

            BoardPlayer2 = new Board(player2);
            BoardPlayer2.CreateAllBoats();
        }        

        protected char GetValueToDraw(
            int x, 
            int y, 
            Board board, 
            bool showAllBoats = false)
        {
            var value = default(char);

            var boatCoordinate = board.GetAllBoatsCoordinatesWithBoat()
                        .Where(c => c.Coordinate.GetCoordinateX() == x
                            && c.Coordinate.GetCoordinateY() == y)
                        .FirstOrDefault();

            var hasAttack = board.GetAttackCoordinates()
                .Any(c => c.GetCoordinateX() == x
                    && c.GetCoordinateY() == y);

            if (boatCoordinate != null
                && hasAttack
                && !showAllBoats)
            {
                value = HIT_BOAT_VALUE;
            }
            else if ((hasAttack && !showAllBoats) || (hasAttack && boatCoordinate is null && showAllBoats))
                value = HIT_SEA_VALUE;
            else if (boatCoordinate != null && showAllBoats)
                value = boatCoordinate.Boat.GetValue();
            else
                value = SEA_VALUE;

            return value;
        }
    }
}
