﻿using NavalBattle.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NavalBattle
{
    public class BoardEngine
    {
        private readonly BoardRender Render;

        public BoardEngine(BoardRender render)
        {
            Render = render;
        }

        public bool IsGameOver()
        {
            foreach(var player in GetPlayers())
            {
                if (Render.GetPlayerBoard(player).GetAllBoats().All(b => b.WasDestroyed()))
                    return true;
            }

            return false;
        }

        public EnumPlayers GetWinnerPlayer()
        {
            var playersLoosers = new List<EnumPlayers>();

            foreach (var player in GetPlayers())
            {
                if (Render.GetPlayerBoard(player)
                        .GetAllBoats()
                        .All(b => b.WasDestroyed()))
                    playersLoosers.Add(player);
            }

            var winners = GetPlayers().Where(p => !playersLoosers.Contains(p));

            return winners.First();
        }

        public void CheckAttack(
            Board board, 
            Coordinate coordinate)
        {
            board.Attack(coordinate);

            var coordinatesAnyBoat = board.GetAllBoatsCoordinatesWithBoat()
                .Where(c => c.Coordinate.GetCoordinateX() == coordinate.GetCoordinateX()
                    && c.Coordinate.GetCoordinateY() == coordinate.GetCoordinateY());

            if (coordinatesAnyBoat.Any())
            {
                var boatPositionCoordinates = coordinatesAnyBoat.First().Boat.GetPosition();

                foreach(var boatPosition in boatPositionCoordinates)
                {
                    var hitBoat =
                        board.GetAttackCoordinates()
                            .Any(a => a.GetCoordinateX() == boatPosition.GetCoordinateX()
                                && a.GetCoordinateY() == boatPosition.GetCoordinateY());

                    if (!hitBoat)
                        return;
                }

                coordinatesAnyBoat.First().Boat.Destroy();
            }
        }

        private EnumPlayers[] GetPlayers()
        {
            return (EnumPlayers[])Enum.GetValues(typeof(EnumPlayers));
        }
    }
}
