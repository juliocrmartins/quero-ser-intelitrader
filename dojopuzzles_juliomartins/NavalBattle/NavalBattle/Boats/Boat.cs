﻿using NavalBattle.Model;
using System.Collections.Generic;

namespace NavalBattle.Boats
{
    public abstract class Boat
    {
        private List<Coordinate> Position;
        private bool Destroyed;

        public abstract int GetLength();
        public abstract char GetValue();
        
        public void Destroy()
        {
            Destroyed = true;
        }

        public bool WasDestroyed()
        {
            return Destroyed;
        }

        public void SetPosition(List<Coordinate> position)
        {
            Position = position;
        }

        public List<Coordinate> GetPosition()
        {
            return Position;
        }
    }
}
