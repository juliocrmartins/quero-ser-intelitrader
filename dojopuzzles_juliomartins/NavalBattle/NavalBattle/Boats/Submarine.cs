﻿namespace NavalBattle.Boats
{
    public class Submarine : Boat
    {
        private const int LENGTH = 3;
        private const char VALUE = 'S';

        public override int GetLength()
        {
            return LENGTH;
        }

        public override char GetValue()
        {
            return VALUE;
        }
    }
}
