﻿namespace NavalBattle.Boats
{
    public class Destroyer : Boat
    {
        private const int LENGTH = 3;
        private const char VALUE = 'D';

        public override int GetLength()
        {
            return LENGTH;
        }

        public override char GetValue()
        {
            return VALUE;
        }
    }
}
