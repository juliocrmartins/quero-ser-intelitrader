﻿namespace NavalBattle.Boats
{
    public class AircraftCarrier : Boat
    {
        private const int LENGTH = 5;
        private const char VALUE = 'P';

        public override int GetLength()
        {
            return LENGTH;
        }

        public override char GetValue()
        {
            return VALUE;
        }
    }
}
