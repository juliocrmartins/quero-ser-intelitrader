﻿namespace NavalBattle.Boats
{
    public class Battleship : Boat
    {
        private const int LENGTH = 4;
        private const char VALUE = 'E';

        public override int GetLength()
        {
            return LENGTH;
        }

        public override char GetValue()
        {
            return VALUE;
        }
    }
}
