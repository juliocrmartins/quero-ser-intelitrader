﻿namespace NavalBattle.Boats
{
    public class PatrolBoat : Boat
    {
        private const int LENGTH = 2;
        private const char VALUE = 'B';

        public override int GetLength()
        {
            return LENGTH;
        }

        public override char GetValue()
        {
            return VALUE;
        }
    }
}
